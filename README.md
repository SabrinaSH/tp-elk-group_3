# Project

Simply a script that parses a csv file and indexes its content in Elastic search after converting it to JSON

# Prerequisites

* Docker
* docker-compose
* npm
* node


# Todo to launch

* Clone the git repo [repo](https://gitlab.com/SabrinaSH/tp-elk-group_3.git)
* cd to projects folder
* Up your docker containers by running : docker-compose up -d ... wait till it's completely up
* If you are on windows with docker toolbox just run the command : npm run launch and that's it
* if not, please go to data.js file: line 5, instead of ip adress put : localhost... the only run 'npm run launch'





