const fs = require('fs');

//load csv file
var data = fs.readFileSync('Data.csv');
data = data.toString();
//devided to lines
var lines=data.split("\n");

  var result = [];
//define headers
  var headers=["title","seo_title","url","author","date","category","locales","content"];
  var separators = [';',';;', ';;;'];
//foreach line split each time we meet previously defined separators
  for(var i=1;i<lines.length;i++){

	  var obj = {};
      var currentline = lines[i].split(new RegExp(separators.join('|'), 'g'));

	  for(var j=0;j<headers.length;j++){
		  obj[headers[j]] = currentline[j];
	  }

	  result.push(obj);

  }
  
  
  fs.writeFile("data.json",JSON.stringify(result),"utf-8",(err) => {
    if(err) console.log(err)
})